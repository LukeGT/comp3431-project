# Notes

- Running RViz while running this project will cause the jaco arm driver to *crash intermittently*.  *Do not use RViz*.  
- This has only been tested on ros groovy, but may work on hydro also. 
- This project is divided into two workspaces, due to the fact that the jaco ros package is in rosbuild workspace format, and we wish to work in a catkin workspace format.  The following page was followed in order to create a ["rosbuild workspace on top of a catkin workspace"](http://wiki.ros.org/catkin/Tutorials/using_rosbuild_with_catkin).  

# Installation

1. Build your catkin workspace

        cd catkin_ws
        catkin_make

2. Install `python-rosinstall`.  It contains the `rosws` command, which you will require.  

        sudo apt-get install python-rosinstall

3. Set up a new workspace named `rosbuild_ws` next to the existing `catkin_ws`. 

        rosws init rosbuild_ws catkin_ws/devel
        source rosbuild_ws/setup.bash

4. Add the following to your `.bashrc` so that you can access both workspaces at the same time:

        source /opt/ros/groovy/setup.bash
        source <project directory>/rosbuild_ws/setup.bash

5. Make sure that rosdep is initialised and updated

        sudo rosdep init
        rosdep update

6. Install ROS Jaco.  

        roscd
        rosws set ros-jaco-arm --git https://github.com/Kinovarobotics/jaco-ros.git
        rosws update ros-jaco-arm
        source setup.bash
        cd ros-jaco-arm
        rosmake

7. Copy all of the files in `rosbuild_ws/ros-jaco-arm/jaco_driver/lib/i386-linux-gnu` to `~/.ros` for linking to work properly.  

        cp jaco_driver/lib/i386-linux-gnu/* ~/.ros

8. Copy `rosbuild_ws/ros-jaco-arm/jaco_driver/udev/99-jaco-arm.rules` into `/etc/udev/rules.d` to ensure that you have write access when the Jaco arm is plugged in

        sudo cp jaco_driver/udev/99-jaco-arm.rules /etc/udev/rules.d/

# Initialisation and Calibration

1. Plug the arm into the computer via USB.  Verify with `lsusb` that a device with vendor id `22cd` appears.  
2. Launch the arm's drivers.  The arm should home, and flex it's fingers.  

        roslaunch jaco_driver jaco_arm.launch

3. Plug in a Kinect-esque device and launch it.  If you verify that it's working with rviz, *make sure you close it afterwards*.  

        roslaunch openni_launch openni.launch

4. Launch the project's nodelets.  Ensure that the Kinect can only see background objects, since a calibration is performed on startup.  This can be re-run by calling the `/reset_background` service.  

        roslaunch project nodelets.launch

5. Place a yellow tennis ball firmly into the gripper of the arm.  You can open its fingers by calling the `/release` service, and close them with the `/grab` service.  

6. Begin automated localisation calibration.  

        rosservice call /jaco/home_arm
        rosservice call /calibrate

    Note that this will put the arm through a predefined series of movements which provided a good calibration for our own particular setup.  If results are poor, then you must perform a manual calibration by selecting a series of positions for the arm to visit between beginning and ending calibration recording.  An example is given below.  The chosen points should vary as much as possible for good results.  

        rosservice call /jaco/home_arm
        rosservice call /begin_recording
        rosservice call /move_arm "position: {x: 0.4, y: 0.4, z: 0}"
        rosservice call /move_arm "position: {x: 0.5, y: 0.0, z: 0}"
        rosservice call /move_arm "position: {x: 0.0, y: 0.5, z: 0}"
        rosservice call /jaco/home_arm
        rosservice call /end_recording

    You can re-run any or all of the calibration steps again if you feel calibration was performed poorly at any stage.  

# Operation

The arm will make auomated decisions depending on what "mood" it is in.  You can change the mood with the following command:

    rosservice call "/change_mood" 'mood: "curious"'

The following moods are defined:

- `obedient`: The arm will perform no automated actions, but will listen to your own instructions.  
- `greedy`: The arm will grab objects placed in front of the Kinect, and will keep them.  
- `ocd`: The arm will clear the table of objects by droppping them off at a predefined location.  Note that this location may be unsafe, depending on your setup.  
- `angry`: The arm will pick up objects and then drop them from the home position
- `curious`: The arm will pick up objects, take them to the home position, then place them back where it found them.  
- `creative`: The arm will attempt to put the shortest object on top of the tallest object, repeatedly, eventually creating a tower of objects. 

There are also a number of commands that you can call, some of which are mentioned above in the calibration steps.  They are `/grab`, `/release`, `/move_arm`, `/pickup`, and `/put_down`.  