#include <ros/ros.h>
#include <iostream>
#include <nodelet/nodelet.h>
#include <std_msgs/Header.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/Marker.h>
#include <pluginlib/class_list_macros.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/registration/icp.h>
#include <tf/transform_datatypes.h>
#include <project/CalculatePosition.h>
#include <project/Objects.h>
#include <vector>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Point.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <project/BeginRecording.h>
#include <project/EndRecording.h>
#include <project/CameraToJaco.h>


#include <Eigen/Core>

typedef project::Objects Objects;
typedef geometry_msgs::PoseStamped PoseStamped;
typedef message_filters::sync_policies::ApproximateTime<Objects, PoseStamped> SyncPolicy;
typedef message_filters::Synchronizer<SyncPolicy> ApproximateSync;

const int CONFIDENT_THRESHOLD = 10;

typedef tf::Point Point;

class JacoLocaliser : public nodelet::Nodelet {

private:

	ros::NodeHandle nodeHandle;

    pcl::PointCloud<pcl::PointXYZ>::Ptr fromCamera;
    pcl::PointCloud<pcl::PointXYZ>::Ptr fromJaco;

    message_filters::Subscriber<project::Objects> foreground_sub;
    message_filters::Subscriber<PoseStamped> jaco_sub;
    ApproximateSync sync;

    ros::ServiceServer beginServer;
    ros::ServiceServer endServer;
    ros::ServiceServer calculationServer;

	ros::Publisher tip_publisher;
	ros::Publisher from_camera_publisher, from_jaco_publisher;

    bool recording;
    
    Eigen::Matrix4f transform;
public:

	JacoLocaliser() :
        fromCamera(new pcl::PointCloud<pcl::PointXYZ>),
        fromJaco (new pcl::PointCloud<pcl::PointXYZ>),
        foreground_sub(nodeHandle, "/foreground_objects", 10),
        jaco_sub(nodeHandle, "/jaco/tool_position", 10),
        sync(SyncPolicy(20), foreground_sub, jaco_sub),
        recording(false)
    {}

	void onInit() {

		ROS_INFO("JacoLocaliser up and running...");

        sync.registerCallback(boost::bind(&JacoLocaliser::onAllData, this, _1, _2));
        
		tip_publisher = nodeHandle.advertise<visualization_msgs::Marker>("/tip", 1);

        beginServer = nodeHandle.advertiseService("/begin_recording", &JacoLocaliser::beginRecording, this);
        endServer = nodeHandle.advertiseService("/end_recording", &JacoLocaliser::endRecording, this);
        calculationServer = nodeHandle.advertiseService("/camera_to_jaco", &JacoLocaliser::cameraToJaco, this);

		from_camera_publisher = nodeHandle.advertise<sensor_msgs::PointCloud2>("/from_camera", 1);
		from_jaco_publisher = nodeHandle.advertise<sensor_msgs::PointCloud2>("/from_jaco", 1);

        transform = Eigen::Matrix4f::Zero();
	}

    bool beginRecording(project::BeginRecordingRequest &request, project::BeginRecordingResponse &response) {
        
        ROS_INFO("Beginning recording.");
        
        fromCamera->clear();
        fromJaco->clear();
        transform = Eigen::Matrix4f::Zero();

        recording = true;
        
        return true;
    }

    bool endRecording(project::EndRecordingRequest &request, project::EndRecordingResponse &response) {
        
        ROS_INFO("Ending recording. Recorded %d points. ", fromCamera->points.size());
        
        recording = false;
        
        return true;
    }

    bool cameraToJaco(project::CameraToJacoRequest &request, project::CameraToJacoResponse &response) {
        
        ROS_INFO("Transforming from camera coordinates to jaco origin coordinates...");

        Eigen::Vector4f p(request.position.x, request.position.y, request.position.z, 1);
        if (transform != Eigen::Matrix4f::Zero()) {
            Eigen::Vector4f r = transform * p;
            response.position.x = r(0);
            response.position.y = r(1);
            response.position.z = r(2);
        }
  
        return true;
    }

    Eigen::Matrix4f calcTransform(int idx[4]) {
        Eigen::Matrix4f c; // Camera coordinates
        Eigen::Matrix4f j; // Jaco coordinates

        for (int i = 0; i < 4; i++) {
            c(0, i) = fromCamera->points[idx[i]].x;
            c(1, i) = fromCamera->points[idx[i]].y;
            c(2, i) = fromCamera->points[idx[i]].z;
            c(3, i) = 1;
            j(0, i) = fromJaco->points[idx[i]].x;
            j(1, i) = fromJaco->points[idx[i]].y;
            j(2, i) = fromJaco->points[idx[i]].z;
            j(3, i) = 1;
        }

       // std::cout << c << std::endl;
       // std::cout << j << std::endl;

        Eigen::Matrix4f t; // Camera to Jaco transform
        t = j * c.inverse();
        if (t(3,3)) {
            t /= t(3, 3);
        } else {
            //ROS_INFO("Transform failed as scaling factor is zero %d %d %d %d", idx[0], idx[1], idx[2], idx[3]);
            //std::cout << t << std::endl;
            return Eigen::Matrix4f::Zero();
        }

        for (int i = 0; i < 3; i++) {
            // This shouldn't affect anything anyway
            t(3, i) = 0;
        }

        return t;
    }

    Eigen::Vector4f toEigen(pcl::PointXYZ p) {
        return Eigen::Vector4f(p.x, p.y, p.z, 1);
    }

    double lenVec(Eigen::Vector4f v) {
        v(3) = 0;
        return v.norm();
    }

    void ransacTransform() {
        if (fromCamera->points.size() < 20) {
            ROS_INFO("Not enough points to start ransac");
            return;
        }
 
        int iterations = 2000;
        bool happy = false;
        int maxHappy = 0;

        while (iterations-- && !happy) {
            // Pick 4 random points
            int idx[4];
            for (int i = 0; i < 4; i++) {
                idx[i] = rand() % (fromCamera->points.size() - i);
                for (int j = 0; j < i; j++) {
                    if (idx[j] <= idx[i]) {
                        idx[i]++;
                    }
                }
            }
            // Get transform matrix
            Eigen::Matrix4f t = calcTransform(idx);
            if (t == Eigen::Matrix4f::Zero()) {
                // DOESN'T COUNT
                iterations++;
                continue;
            }

            // Find how many vectors match this kinda
            int matches = 0;
            for (int i = 0; i < fromCamera->points.size(); i++) {
                if (lenVec(t * toEigen(fromCamera->points[i]) - toEigen(fromJaco->points[i])) < 0.015) {
                    matches++;
                }
            }

            // If better than threshold, happy!
            if (matches > maxHappy) {
                maxHappy = matches;
            }
            if (matches > fromCamera->points.size() * 0.80) {
                happy = true;
                transform = t;
            }
        }

        if (happy) {
            std::cout << transform << std::endl;
            Eigen::Vector4f forward1 = Eigen::Vector4f::UnitZ();
            Eigen::Vector4f origin = Eigen::Vector4f::Zero();
            forward1.w() = 1;
            origin.w() = 1;
            std::cout << (transform * forward1) << std::endl;
            std::cout << (transform * origin) << std::endl;
        } else {
            ROS_INFO("Ransac failed with %d points, maxHappy = %d", fromCamera->points.size(), maxHappy);
        }
    }

    void onAllData(const project::ObjectsConstPtr& obInput, const geometry_msgs::PoseStampedConstPtr& jacoInput) {

        if (!recording) return;

        const geometry_msgs::Point &p = jacoInput->pose.position;

        for (int i = 0; i < obInput->objects.size(); i++) {
            ROS_INFO("type string is %s", obInput->objects[i].type.c_str());
            if (obInput->objects[i].type == "hand") {
                // arm point
                pcl::PointXYZ ap(
                        obInput->objects[i].pose.position.x,
                        obInput->objects[i].pose.position.y,
                        obInput->objects[i].pose.position.z
                        );

                fromCamera->points.push_back(ap);
                fromJaco->points.push_back(pcl::PointXYZ(p.x, p.y, p.z));

                publishTip(ap, obInput->header);

                ROS_INFO("info #%d, matching camera (%lf, %lf, %lf) with jaco (%lf, %lf, %lf)", fromCamera->points.size(), ap.x, ap.y, ap.z, p.x, p.y, p.z);
            }
        }

        ransacTransform();

		sensor_msgs::PointCloud2 from_msg;
		pcl::toROSMsg(*fromCamera, from_msg);
        from_msg.header = obInput->header;
        from_msg.header.frame_id = "/jaco_api_origin";
		from_camera_publisher.publish(from_msg);

		pcl::toROSMsg(*fromJaco, from_msg);
        from_msg.header = obInput->header;
        from_msg.header.frame_id = "/jaco_api_origin";
		from_jaco_publisher.publish(from_msg);
    }

    void publishTip(pcl::PointXYZ tip, std_msgs::Header header) {
        visualization_msgs::Marker marker;
		marker.header.frame_id = header.frame_id;
		marker.header.stamp = header.stamp;
		marker.ns = "tip";
		marker.id = 0;
		marker.type = visualization_msgs::Marker::SPHERE;
		marker.action = visualization_msgs::Marker::ADD;
		marker.pose.position.x = tip.x;
		marker.pose.position.y = tip.y;
		marker.pose.position.z = tip.z;
		marker.pose.orientation.x = 0.0;
		marker.pose.orientation.y = 0.0;
		marker.pose.orientation.z = 0.0;
		marker.pose.orientation.w = 1.0;
		marker.scale.x = 0.01;
		marker.scale.y = 0.01;
		marker.scale.z = 0.01;
		marker.color.a = 1.0;
		marker.color.r = 0.0;
		marker.color.g = 1.0;
		marker.color.b = 0.0;
		tip_publisher.publish(marker);
    }
};

PLUGINLIB_DECLARE_CLASS(0, 0, JacoLocaliser, nodelet::Nodelet) 
