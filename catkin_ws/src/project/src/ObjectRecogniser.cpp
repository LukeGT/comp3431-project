#include <ros/ros.h>
#include <iostream>
#include <nodelet/nodelet.h>
#include <sensor_msgs/PointCloud2.h>
#include <pluginlib/class_list_macros.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <vector>
#include <geometry_msgs/Pose.h>
#include <project/Objects.h>
#include <project/Clusters.h>
#include "HelperFunctions.h"
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>


typedef pcl::PointCloud<pcl::PointXYZRGB> pCloud;

class ObjectRecogniser : public nodelet::Nodelet {

private:

	ros::NodeHandle nodeHandle;
	ros::Subscriber subscriber;
	ros::Publisher publisher;
    ros::Publisher centres_publisher;
    Eigen::Vector3f greenCenter;
    int max_markers;
public:

    ObjectRecogniser() {}

    void onInit() {
        ROS_INFO("ObjectRecogniser up and running...");

        max_markers = 0;
        subscriber = nodeHandle.subscribe("/foreground_clusters", 1, &ObjectRecogniser::onClustersData, this);
        publisher = nodeHandle.advertise<project::Objects>("/foreground_objects", 1);
		centres_publisher = nodeHandle.advertise<visualization_msgs::MarkerArray>("/tip_green", 1);
    }

    void deleteTip(visualization_msgs::MarkerArray &array, std_msgs::Header header) {
        visualization_msgs::Marker marker;
		marker.header.frame_id = header.frame_id;
		marker.header.stamp = header.stamp;
		marker.ns = "tip_green";
		marker.id = array.markers.size();
        marker.action = visualization_msgs::Marker::DELETE;
        array.markers.push_back(marker);
    }

    void addTip(Eigen::Vector3f tip, visualization_msgs::MarkerArray &array, std_msgs::Header header) {
        visualization_msgs::Marker marker;
		marker.header.frame_id = header.frame_id;
		marker.header.stamp = header.stamp;
		marker.ns = "tip_green";
		marker.id = array.markers.size();
		marker.type = visualization_msgs::Marker::SPHERE;
		marker.action = visualization_msgs::Marker::ADD;
		marker.pose.position.x = tip.x();
		marker.pose.position.y = tip.y();
		marker.pose.position.z = tip.z();
		marker.pose.orientation.x = 0.0;
		marker.pose.orientation.y = 0.0;
		marker.pose.orientation.z = 0.0;
		marker.pose.orientation.w = 1.0;
		marker.scale.x = 0.01;
		marker.scale.y = 0.01;
		marker.scale.z = 0.01;
		marker.color.a = 1.0;
		marker.color.r = 1.0;
		marker.color.g = 0.0;
		marker.color.b = 0.0;
        array.markers.push_back(marker);
    }


    bool isHand(pCloud &data, project::Object &obj) {
        int greenCount = 0;
        Eigen::Vector3f greenCen = Eigen::Vector3f::Zero();
        for (int i = 0; i < data.points.size(); i++) {
            pcl::PointXYZRGB &p = data.points[i];
            Eigen::Vector3f d(p.r / 255.0, p.g / 255.0, p.b / 255.0);
            Eigen::Vector3f c = rgb2hsv(d);
            if (c.x() >= 55.0 && c.x() <= 75.0 && c.y() >= 0.35 && c.y() <= 1.0 && c.z() >= 0.25 && c.z() <= 0.9) {
                greenCount++;
                greenCen += Eigen::Vector3f(p.x, p.y, p.z);
            }
        }
        if (greenCount >= 1000) {
            greenCen /= greenCount;
            greenCen += (greenCen.normalized()) * 0.025;
            //printf ("%d, %lf %lf %lf\n", greenCount, greenCen.x(), greenCen.y(), greenCen.z());
            greenCenter = greenCen;
        }
        return greenCount >= 1000;
    }



    void onClustersData(const project::ClustersConstPtr& input) {
        project::Objects objects;
        visualization_msgs::MarkerArray array;
        objects.header = input->header;
        for (int i = 0; i < input->clusters.size(); i++) {
            Eigen::Vector3f total;

            pCloud in;
            pcl::fromROSMsg(input->clusters[i], in);

            if (in.size() == 0) {
                continue;
            }

            //ROS_INFO("in size = %d", in.size());

            for (std::vector<pcl::PointXYZRGB, Eigen::aligned_allocator<pcl::PointXYZRGB> >::const_iterator i = in.begin(); i != in.end(); i++) {
                total += Eigen::Vector3f(i->x, i->y, i->z);
            }

            total /= in.points.size();

            project::Object object;


            if (isHand(in, object)) {
                object.type = "hand";
                total = greenCenter;
                addTip(greenCenter, array, input->header);
            } else {
                object.type = "cylinder";
                // First, work out the radius
                float maxNorm = -1.0;
                float minY = 100.0;
                for (int i = 0; i < in.points.size(); i++) {
                    pcl::PointXYZRGB &p = in.points[i];
                    Eigen::Vector3f e(p.x, p.y, p.z);
                    Eigen::Vector3f dif = e - total;
                    dif.y() = 0;
                    if (dif.norm() > maxNorm) {
                        maxNorm = dif.norm();
                    }
                    if (p.y < minY) {
                        minY = p.y;
                    }
                }
                total.y() = 0;
                total += (total.normalized() * maxNorm * 0.43);
                total.y() = minY;
                addTip(total, array, input->header);
            }
            float minY = 100.0;
            float maxY = -100.0;
            for (int i = 0; i < in.points.size(); i++) {
                pcl::PointXYZRGB &p = in.points[i];
                if (p.y < minY) {
                    minY = p.y;
                }
                if (p.y > maxY) {
                    maxY = p.y;
                }
            }
            object.height = maxY - minY;
            object.pose.position.x = total.x();
            object.pose.position.y = total.y();
            object.pose.position.z = total.z();
            objects.objects.push_back(object);
        }
        int amo_pub = array.markers.size();
        while (array.markers.size() < max_markers) {
            deleteTip(array, input->header);
        }
        max_markers = amo_pub;
        centres_publisher.publish(array);
        publisher.publish(objects);
    }
};

PLUGINLIB_DECLARE_CLASS(0, 0, ObjectRecogniser, nodelet::Nodelet) 
