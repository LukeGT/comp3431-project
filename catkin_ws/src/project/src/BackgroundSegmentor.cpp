#include "BackgroundSegmentor.h"
#include <ctime>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;


#define INF					(X_SIZE * Y_SIZE * Z_SIZE * 2)
#define TOTAL_SIZE			(X_SIZE * Y_SIZE * Z_SIZE)

//#define REPORT_TIMES

#ifdef REPORT_TIMES
static clock_t _tO;

static void init_clock(void) {
	_tO = clock();
}

static double lap(void) {
	double res = ((double) clock() - _tO) / CLOCKS_PER_SEC;
	_tO = clock();
	return res;
}

static void output_time(void) {
	printf ("Time %lf\n", lap());
}
#else
static void init_clock(void){}
static void output_time(void){}
#endif


point extractLoc(const pcl::PointXYZRGB &p) {
	point res;
	if (p.x < BOUND_MIN_X || p.x > BOUND_MAX_X ||
			p.y < BOUND_MIN_Y || p.y > BOUND_MAX_Y ||
			p.z < BOUND_MIN_Z || p.z > BOUND_MAX_Z) {
		res.x = -1;
		return res;
	}
	res.x = (int) ((p.x - BOUND_MIN_X) * RESOLUTION);
	res.y = (int) ((p.y - BOUND_MIN_Y) * RESOLUTION);
	res.z = (int) ((p.z - BOUND_MIN_Z) * RESOLUTION);
	return res;
}

BackgroundSegmentor::BackgroundSegmentor() {
	init_clock();
	// Malloc time!
	isPoint = new bool **[X_SIZE];
	distances = new int **[X_SIZE];
	for (int i = 0; i < X_SIZE; i++) {
		isPoint[i] = new bool *[Y_SIZE];
		distances[i] = new int *[Y_SIZE];
		for (int j = 0; j < Y_SIZE; j++) {
			isPoint[i][j] = new bool[Z_SIZE];
			distances[i][j] = new int[Z_SIZE];
		}
	}
	isFinalised = false;
	output_time();
}

BackgroundSegmentor::~BackgroundSegmentor() {
	for (int i = 0; i < X_SIZE; i++) {
		for (int j = 0; j < Y_SIZE; j++) {
			delete[] isPoint[i][j];
			delete[] distances[i][j];
		}
		delete[] isPoint[i];
		delete[] distances[i];
	}
	delete[] isPoint;
	delete[] distances;
}

void BackgroundSegmentor::addBackground(const pcl::PointCloud<pcl::PointXYZRGB> &cloud) {
	if (isFinalised) {
		ROS_ERROR("Attempted to add background to finalised set\n");
		return;
	}
	init_clock();
	point p;
	for (int i = 0; i < cloud.points.size(); i++) {
		if (isnan(cloud.points[i].x)) continue;
		p = extractLoc(cloud.points[i]);
		if (p.x == -1) continue;
		isPoint[p.x][p.y][p.z] = true;
	}
	output_time();
}

void BackgroundSegmentor::finialiseBackground(void) {
	if (isFinalised) {
		ROS_ERROR("Attempted to finalised already finalised set\n");
		return;
	}
	init_clock();
	for (int i = 0; i < X_SIZE; i++) {
		for (int j = 0; j < Y_SIZE; j++) {
			for (int k = 0; k < Z_SIZE; k++) {
				distances[i][j][k] = INF;
			}
		}
	}
	output_time();
	queue<point> q;
	for (int i = 0; i < X_SIZE; i++) {
		for (int j = 0; j < Y_SIZE; j++) {
			for (int k = 0; k < Z_SIZE; k++) {
				if (isPoint[i][j][k]) {
					point p;
					p.x = i;
					p.y = j;
					p.z = k;
					q.push(p);
					distances[i][j][k] = 0;
				}
			}
		}
	}
	output_time();
	int dx[] = {1, -1, 0, 0, 0, 0};
	int dy[] = {0, 0, 1, -1, 0, 0};
	int dz[] = {0, 0, 0, 0, 1, -1};
	int cX, cY, cZ;
	int count = 0;
	while (q.size() > 0) {
		point c = q.front();
		q.pop();
		for (int i = 0; i < 6; i++) {
			cX = c.x + dx[i];
			cY = c.y + dy[i];
			cZ = c.z + dz[i];
			if (cX >= 0 && cX < X_SIZE && cY >= 0 && cY < Y_SIZE && cZ >= 0 && cZ < Z_SIZE) {
				if (distances[cX][cY][cZ] == INF) {
					distances[cX][cY][cZ] = distances[c.x][c.y][c.z] + 1;
					point v;
					v.x = cX;
					v.y = cY;
					v.z = cZ;
					q.push(v);
				}
			}
		}
		count++;
		if (count % (TOTAL_SIZE / 10) == 0) {
			printf ("Finalised %d%%\n", (count / (TOTAL_SIZE / 100)));
		}
	}
	output_time();
	isFinalised = true;
}

const float BackgroundSegmentor::queryPoint(const pcl::PointXYZRGB &p) {
	if (!isFinalised) {
		ROS_ERROR("Attempting to query a non-finalised set\n");
		return 0;
	}
	point pos = extractLoc(p);
	if (pos.x == -1 || distances[pos.z][pos.y][pos.z] == INF) {
		return 0;
	} else {
		return ((float) distances[pos.x][pos.y][pos.z] / RESOLUTION);
	}
}

void BackgroundSegmentor::resetBackground(void) {
	for (int i = 0; i < X_SIZE; i++) {
		for (int j = 0; j < Y_SIZE; j++) {
			for (int k = 0; k < Z_SIZE; k++) {
				distances[i][j][k] = INF;
				isPoint[i][j][k] = false;
			}
		}
	}
	isFinalised = false;
}



