#include "HelperFunctions.h"


Eigen::Vector3f rgb2hsv(Eigen::Vector3f in) {
    Eigen::Vector3f out;
    float minV, maxV, delta;

    minV = in.x() < in.y() ? in.x() : in.y();
    minV = minV < in.z() ? minV : in.z();
    maxV = in.x() > in.y() ? in.x() : in.y();
    maxV = maxV > in.z() ? maxV : in.z();

    out.z() = maxV;                                // v
    delta = maxV - minV;
    if (maxV > 0.0) {
        out.y() = (delta / maxV);                  // s
        if (delta <= 0.0) {
            out.x() = 0.0;
            return out;
        }
    } else {
        // r = g = b = 0                        // s = 0, v is undefined
        out.z() = 0.0;
        out.y() = 0.0;                            // its now undefined
        out.x() = 0.0;
        return out;
    }
    if (in.x() >= maxV) {
        out.x() = (in.y() - in.z()) / delta;        // between yellow & magenta
    } else {
        if (in.y() >= maxV) {
            out.x() = 2.0 + (in.z() - in.x()) / delta;  // between cyan & yellow
        } else {
            out.x() = 4.0 + (in.x() - in.y()) / delta;  // between magenta & cyan
        }
    }
    out.x() *= 60.0;                              // degrees
    if (out.x() < 0.0 ) {
        out.x() += 360.0;
    }
    return out;
}


