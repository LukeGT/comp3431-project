#ifndef BACKGROUND_SEGMENTOR
#define BACKGROUND_SEGMENTOR

#include <pcl/point_cloud.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <ros/ros.h>

#define BOUND_MIN_X			(-2.0)
#define BOUND_MAX_X			(2.0)
#define BOUND_MIN_Y			(-0.5)
#define BOUND_MAX_Y			(1.5)
#define BOUND_MIN_Z			(0.0)
#define BOUND_MAX_Z			(3.0)

#define RESOLUTION          (100) // Points per metre
#define X_SIZE              ((int) ((BOUND_MAX_X - BOUND_MIN_X) * RESOLUTION))
#define Y_SIZE              ((int) ((BOUND_MAX_Y - BOUND_MIN_Y) * RESOLUTION))
#define Z_SIZE              ((int) ((BOUND_MAX_Z - BOUND_MIN_Z) * RESOLUTION))

#define MAX_DIS				(X_SIZE + Y_SIZE + Z_SIZE)

typedef struct _point point;

struct _point {
	int x;
	int y;
	int z;
	int ind;
	int other_ind;
	int next_ind;
};

point extractLoc(const pcl::PointXYZRGB &p);

class BackgroundSegmentor {
	private: 	
		bool ***isPoint;
		int  ***distances;
		bool isFinalised;
	public:
		BackgroundSegmentor();
		~BackgroundSegmentor();
		void addBackground(const pcl::PointCloud<pcl::PointXYZRGB> &cloud);
		void finialiseBackground(void);
		const float queryPoint(const pcl::PointXYZRGB &p);
		void resetBackground(void);
};

#endif


