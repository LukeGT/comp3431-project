#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <std_msgs/Header.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/Marker.h>
#include <pluginlib/class_list_macros.h>
#include <project/ObjectPresent.h>
#include <project/CalculatePosition.h>
#include <pcl/point_cloud.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/extract_indices.h>

class PositionCalculator : public nodelet::Nodelet {

private:

	ros::NodeHandle nodeHandle;

	ros::Subscriber foreground_subscriber;
	ros::Subscriber presence_subscriber;

	ros::Publisher tip_publisher;

	ros::ServiceServer serviceServer;

	bool present;
	std_msgs::Header foreground_header;
	pcl::PointCloud<pcl::PointXYZRGB> foreground;

public:

	PositionCalculator() :
		present(false)
	{}

	void onInit() {

		ROS_INFO("PositionCalculator up and running...");

		foreground_subscriber = nodeHandle.subscribe("/foreground", 1, &PositionCalculator::onForeground, this);
		presence_subscriber = nodeHandle.subscribe("/object_present", 1, &PositionCalculator::onPresence, this);

		tip_publisher = nodeHandle.advertise<visualization_msgs::Marker>("/tip", 1);
		
		serviceServer = nodeHandle.advertiseService("/calculate_position", &PositionCalculator::calculatePosition, this);
	}

	void onForeground(const sensor_msgs::PointCloud2ConstPtr &input) {
		foreground_header = input->header;
		pcl::fromROSMsg(*input, foreground);
	}

	void onPresence(const project::ObjectPresent &presence) {
		present = presence.present;
	}

	bool calculatePosition(project::CalculatePositionRequest &request, project::CalculatePositionResponse &response) {

		ROS_INFO("Calculating position...");

		pcl::PointXYZRGB tip;
		tip.y = -FLT_MAX;

		for (int a = 0; a < foreground.points.size(); ++a) {
			if (foreground.points[a].y > tip.y) {
				tip = foreground.points[a];
			}
		}

		std::cout << tip << '\n';

		visualization_msgs::Marker marker;
		marker.header.frame_id = foreground_header.frame_id;
		marker.header.stamp = foreground_header.stamp;
		marker.ns = "tip";
		marker.id = 0;
		marker.type = visualization_msgs::Marker::SPHERE;
		marker.action = visualization_msgs::Marker::ADD;
		marker.pose.position.x = tip.x;
		marker.pose.position.y = tip.y;
		marker.pose.position.z = tip.z;
		marker.pose.orientation.x = 0.0;
		marker.pose.orientation.y = 0.0;
		marker.pose.orientation.z = 0.0;
		marker.pose.orientation.w = 1.0;
		marker.scale.x = 0.01;
		marker.scale.y = 0.01;
		marker.scale.z = 0.01;
		marker.color.a = 1.0;
		marker.color.r = 0.0;
		marker.color.g = 1.0;
		marker.color.b = 0.0;
		tip_publisher.publish(marker);
	}
};

PLUGINLIB_DECLARE_CLASS(0, 0, PositionCalculator, nodelet::Nodelet)