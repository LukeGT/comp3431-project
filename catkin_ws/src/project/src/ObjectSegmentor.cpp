#include "BackgroundSegmentor.h"
#include "HelperFunctions.h"
#include <algorithm>
#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <sensor_msgs/PointCloud2.h>
#include <pluginlib/class_list_macros.h>
#include <project/ResetBackground.h>
#include <project/Clusters.h>
#include <pcl/point_cloud.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/extract_indices.h>
#include <ctime>
#include <queue>

using namespace std;

#define BACKGROUND_SAMPLE		(5)
#define MIN_POINTS_REQUIRED 	(1000)
#define BOUNDARY_BACK_DIS		(0.02)
#define IMAGE_WIDTH         	(640)
#define IMAGE_HEIGHT        	(480)

#define NO_NEXT					(-1)

typedef pcl::PointCloud<pcl::PointXYZRGB> pCloud;

class cluster {
	public:
		vector<point> points;
		void addPoint(const point &p) {
			points.push_back(p);
		}

		const int size(void) {
			return points.size();
		}
};


clock_t _tO;

double lap(void) {
	double res = ((double) clock() - _tO) / CLOCKS_PER_SEC;
	_tO = clock();
	return res;
}

class ObjectSegmentor : public nodelet::Nodelet {

	private:

		ros::NodeHandle nodeHandle;
		ros::Subscriber subscriber;
		ros::Publisher foreground_publisher;
		ros::Publisher foreground_coloured_publisher;
		ros::Publisher background_publisher;
		ros::Publisher clusters_publisher;
		ros::ServiceServer serviceServer;

		int background_count;
		pCloud background;
		BackgroundSegmentor bs;
		
		int ***isPoint;
		int ***pointsAt;
		int seenUpTo;

	public:

		ObjectSegmentor() :
			background_count(BACKGROUND_SAMPLE)
	{}

		void initArrays() {
			isPoint = new int**[X_SIZE];
			pointsAt = new int**[X_SIZE];
			for (int i = 0; i < X_SIZE; i++) {
				isPoint[i] = new int*[Y_SIZE];
				pointsAt[i] = new int*[Y_SIZE];
				for (int j = 0; j < Y_SIZE; j++) {
					isPoint[i][j] = new int[Z_SIZE];
					pointsAt[i][j] = new int[Z_SIZE];
					for (int k = 0; k < Z_SIZE; k++) {
						isPoint[i][j][k] = 0;
					}
				}
			}
			seenUpTo = 0;
			ROS_INFO("ObjectSegmentor initialised arrays!\n");
		}

		void onInit() {
			ROS_INFO("ObjectSegmentor up and running...");
			initArrays();
			subscriber = nodeHandle.subscribe("/camera/depth_registered/points", 1, &ObjectSegmentor::onPointData, this);
			foreground_publisher = nodeHandle.advertise<sensor_msgs::PointCloud2>("/foreground", 1);
			foreground_coloured_publisher = nodeHandle.advertise<sensor_msgs::PointCloud2>("/foreground_coloured", 1);
			background_publisher = nodeHandle.advertise<sensor_msgs::PointCloud2>("/background", 1);
			clusters_publisher = nodeHandle.advertise<project::Clusters>("/foreground_clusters", 1);
			serviceServer = nodeHandle.advertiseService("/reset_background", &ObjectSegmentor::resetBackground, this);
		}

		bool resetBackground(project::ResetBackgroundRequest &request, project::ResetBackgroundResponse &response) {
			background.clear();
			bs.resetBackground();
			background_count = BACKGROUND_SAMPLE;
		}

		void setBackground(pCloud &cloud) {
			if (background.points.size() == 0) {
				background = cloud;
			} else {
				int times = 0;
				for (int a = 0; a < background.points.size(); ++a) {
					if (!isnan(background.points[a].x)) continue;
					if (!isnan(cloud.points[a].x))  {
						times++;
						background.points[a] = cloud.points[a];
					}
				}
				ROS_INFO("times: %d", times);
			}
			bs.addBackground(cloud);
			//background_kdtree.setInputCloud(background.makeShared());
			sensor_msgs::PointCloud2 background_msg;
			pcl::toROSMsg(background, background_msg);
			background_publisher.publish(background_msg);
		}

		// Returns a vector of points which are not in the background
		void findNonBackgroundPoints(pCloud &cloud, vector<point> &foreground) {
			foreground.clear();
			for (int i = 0; i < cloud.points.size(); i++) {
				if (!isnan(cloud.points[i].x)) {
					if (bs.queryPoint(cloud.points[i]) > BOUNDARY_BACK_DIS) {
						foreground.push_back(extractLoc(cloud.points[i]));
						foreground.back().other_ind = i;
					}
				}
			}
			printf ("Found %d points\n", foreground.size());
		}

		void insertPointsArrays(vector<point> &foreground) {
			// First, increment seen thing
			seenUpTo++;
			for (int i = 0; i < foreground.size(); i++) {
				point &p = foreground[i];
				p.ind = i;
				p.next_ind = NO_NEXT;
				if (isPoint[p.x][p.y][p.z] == seenUpTo) {
					p.next_ind = pointsAt[p.x][p.y][p.z];
				} else {
					isPoint[p.x][p.y][p.z] = seenUpTo;
				}
				pointsAt[p.x][p.y][p.z] = i;
			}
		}

		void createCluster(vector<point> &foreground, vector<cluster> &objects, point &start) {
			queue<int> q;
			objects.push_back(cluster());
			cluster &c = objects.back();
			point &cur = start;
			while (true) {
				q.push(cur.ind);
				if (cur.next_ind == NO_NEXT) {
					break;
				}
				cur = foreground[cur.next_ind];
			}
			isPoint[start.x][start.y][start.z] = seenUpTo - 1;
			while (q.size() > 0) {
				point &p = foreground[q.front()];
				for (int i = -1; i <= 1; i++) {
					for (int j = -1; j <= 1; j++) {
						for (int k = -1; k <= 1; k++) {
							int nX = p.x + i;
							int nY = p.y + j;
							int nZ = p.z + k;
							if (nX >= 0 && nX < X_SIZE && nY >= 0 && nY < Y_SIZE && nZ >= 0 && nZ < Z_SIZE) {
								if (isPoint[nX][nY][nZ] == seenUpTo) {
									isPoint[nX][nY][nZ] = seenUpTo - 1;
									cur = foreground[pointsAt[nX][nY][nZ]];
									while(true) {
										q.push(cur.ind);
										if (cur.next_ind == NO_NEXT) {
											break;
										}
										cur = foreground[cur.next_ind];
									}
								}
							}
						}
					}
				}
				c.addPoint(p);
				q.pop();
			}
			if (c.size() < MIN_POINTS_REQUIRED) {
				objects.pop_back();
			}
		}

		void convertObjToCloud(cluster &c, pCloud::Ptr &org, pCloud &cloud) {
			pcl::PointIndices::Ptr goodInd(new pcl::PointIndices());
			for (int i = 0; i < c.points.size(); i++) {
				goodInd->indices.push_back(c.points[i].other_ind);
			}
			pcl::ExtractIndices<pcl::PointXYZRGB> extractor;
			extractor.setInputCloud(org);
			extractor.setIndices(goodInd);
			extractor.setNegative(false);
			extractor.filter(cloud);
		}

		void onPointData(const sensor_msgs::PointCloud2ConstPtr& input) {
			// Convert to PCL cloud
			pCloud::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
			pcl::fromROSMsg(*input, *cloud);
			// Register the first few point clouds received as the background
			if (background_count > 0) {
				setBackground(*cloud);
				background_count--;
				if (background_count > 0) {
					ROS_INFO("Registering background...");
				} else {
					ROS_INFO("Registered background.");
					bs.finialiseBackground();
				}
				return;
			}
			_tO = clock();
			// Else, we first remove background points
			vector<point> foreground;
			vector<cluster> objects;
			vector<pCloud> clouds;
			findNonBackgroundPoints(*cloud, foreground);

			//Second, convert and add all points into the giant arrays
			insertPointsArrays(foreground);

			//Third, create objects from each point left
			for (int i = 0; i < foreground.size(); i++) {
				point &p = foreground[i];
				if (isPoint[p.x][p.y][p.z] == seenUpTo) {
					createCluster(foreground, objects, p);
				}
			}
			printf ("Found %d objects\n", objects.size());
			//Finally, convert objects into message and send
			for (int i = 0; i < objects.size(); i++) {
				clouds.push_back(pCloud());
				convertObjToCloud(objects[i], cloud, clouds[i]);	
			}
			project::Clusters clusters;
			for (int i = 0; i < clouds.size(); i++) {
				clusters.clusters.push_back(sensor_msgs::PointCloud2());
				pcl::toROSMsg(clouds[i], clusters.clusters[i]);
			}
			clusters.header = input->header;
			clusters_publisher.publish(clusters);

			//If debug, produce debugging output
			vector<int> allInd;
			for (int i = 0; i < objects.size(); i++) {
				for (int j = 0; j < objects[i].points.size(); j++) {
					pcl::PointXYZRGB &p = cloud->points[objects[i].points[j].other_ind];
                    Eigen::Vector3f d(p.r / 255.0, p.g / 255.0, p.b / 255.0);	
                    Eigen::Vector3f c = rgb2hsv(d);
                    if (c.x() >= 55.0 && c.x() <= 75.0 && c.y() >= 0.35 && c.y() <= 1.0 && c.z() >= 0.25 && c.z() <= 0.9) {
                        allInd.push_back(objects[i].points[j].other_ind);
                    }
                }
            }
            pCloud final_foreground(*cloud, allInd);

            sensor_msgs::PointCloud2 foreground_msg;
            pcl::toROSMsg(final_foreground, foreground_msg);
            foreground_publisher.publish(foreground_msg);
            //TODO: Create a point cloud with coloured points
            // First set them in the original cloud
            allInd.clear();
			for (int i = 0; i < objects.size(); i++) {
				for (int j = 0; j < objects[i].points.size(); j++) {
                    allInd.push_back(objects[i].points[j].other_ind);
                }
            }
            int colours[] = {5, 3, 7, 2, 4, 1, 0, 6};
            for (int i = 0; i < objects.size(); i++) {
                int r = 255 * (colours[i % 8] >> 2);
                int g = 255 * ((colours[i % 8] >> 1) % 2);
                int b = 255 * (colours[i % 8] % 2);
                for (int j = 0; j < objects[i].points.size(); j++) {
                    pcl::PointXYZRGB &p = cloud->points[objects[i].points[j].other_ind];
                    p.r = r;
                    p.g = g;
                    p.b = b;
                }
            }
            pCloud foreground_colour(*cloud, allInd);
            sensor_msgs::PointCloud2 foreground_colour_msg;
            pcl::toROSMsg(foreground_colour, foreground_colour_msg);
            foreground_coloured_publisher.publish(foreground_colour_msg);

            ROS_INFO("Took %f to do\n", lap());
        }
};

PLUGINLIB_DECLARE_CLASS(0, 0, ObjectSegmentor, nodelet::Nodelet) 
