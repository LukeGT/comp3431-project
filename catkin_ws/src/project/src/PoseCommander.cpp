#include <ros/ros.h>
#include <cfloat>
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>
#include <project/ArmPoseAction.h>
#include <project/SetFingersPositionAction.h>
#include <project/MoveArm.h>
#include <project/HomeArm.h>
#include <project/Grab.h>
#include <project/Release.h>
#include <project/Pickup.h>
#include <project/PutDown.h>
#include <project/Objects.h>
#include <project/Object.h>
#include <project/ChangeMood.h>
#include <project/CameraToJaco.h>
#include <project/BeginRecording.h>
#include <project/EndRecording.h>
#include <project/Calibrate.h>
#include <actionlib/client/simple_action_client.h>

#define COMMAND_TIMEOUT 15.0

#define OBJECT_HISTORY 5
#define STD_DEV_THRESH 0.001

#define ARM_RAISE 0.02
#define CREATIVE_GAP 0.01

enum Mood {
	OBEDIENT = 0,
	GREEDY,
	OCD,
	ANGRY,
    CURIOUS,
    CREATIVE,
	MOOD_NUM
};

struct ObjectStruct {
	double x;
	double y;
	double z;
    double height;
};

ObjectStruct operator+(ObjectStruct a, ObjectStruct b) {
	
	ObjectStruct objectStruct;
	
	objectStruct.x = a.x + b.x;
	objectStruct.y = a.y + b.y;
	objectStruct.z = a.z + b.z;
    objectStruct.height = a.height + b.height;

	return objectStruct;
}

ObjectStruct structFromObjectMsg(project::Object objectMsg) {
	
	ObjectStruct objectStruct;

	objectStruct.x = objectMsg.pose.position.x;
	objectStruct.y = objectMsg.pose.position.y;
	objectStruct.z = objectMsg.pose.position.z;
    objectStruct.height = objectMsg.height;

	return objectStruct;
}

std::vector<ObjectStruct> structsFromObjectsMsg(project::Objects objectsMsg) {

	std::vector<ObjectStruct> objectStructs;
	
	for (int a = 0; a < objectsMsg.objects.size(); ++a) {
		objectStructs.push_back(structFromObjectMsg(objectsMsg.objects[a]));
	}

	return objectStructs;
}

class PoseCommander : public nodelet::Nodelet {

private:

	ros::NodeHandle nodeHandle;

	actionlib::SimpleActionClient<project::ArmPoseAction> armPoseClient;
	actionlib::SimpleActionClient<project::SetFingersPositionAction> fingerPositionClient;
	
	ros::Subscriber presenceSubscriber;
	ros::Subscriber objectsSubscriber;

	ros::ServiceServer moveServer;
	ros::ServiceServer grabServer;
	ros::ServiceServer releaseServer;
	ros::ServiceServer pickupServer;
	ros::ServiceServer putDownServer;
	ros::ServiceServer changeMoodServer;
	ros::ServiceServer calibrateServer;

	ros::ServiceClient homeArmClient;
	ros::ServiceClient cameraToJacoClient;
	ros::ServiceClient beginRecordingClient;
	ros::ServiceClient endRecordingClient;

	project::FingerPosition previous_fingers;

	ros::Publisher averagePublisher;

	Mood mood;
	std::string moods[MOOD_NUM];

	int object_index;
	int object_history_count;
	std::vector<ObjectStruct> object_history[OBJECT_HISTORY];
	std::vector<ObjectStruct> average;
	std::vector<ObjectStruct> std_dev;
 
public:

	PoseCommander() :
		armPoseClient("/jaco/arm_pose", true),
		fingerPositionClient("/jaco/finger_joint_angles", true),
		mood(OBEDIENT),
		object_index(-1),
		object_history_count(0)
	{
		previous_fingers.Finger_1 = -1;
		previous_fingers.Finger_2 = -1;
		previous_fingers.Finger_3 = -1;

		moods[OBEDIENT] = "obedient";
		moods[GREEDY] = "greedy";
		moods[OCD] = "ocd";
		moods[ANGRY] = "angry";
        moods[CURIOUS] = "curious";
        moods[CREATIVE] = "creative";
	}

	void onInit() {
		
		nodeHandle = getMTNodeHandle();

		ROS_INFO("PoseCommander up and running...");

		objectsSubscriber = nodeHandle.subscribe("/foreground_objects", 1, &PoseCommander::onObjects, this);

		moveServer = nodeHandle.advertiseService("/move_arm", &PoseCommander::moveArm, this);
		grabServer = nodeHandle.advertiseService("/grab", &PoseCommander::grab, this);
		releaseServer = nodeHandle.advertiseService("/release", &PoseCommander::release, this);
		pickupServer = nodeHandle.advertiseService("/pickup", &PoseCommander::pickup, this);
		putDownServer = nodeHandle.advertiseService("/put_down", &PoseCommander::putDown, this);
		changeMoodServer = nodeHandle.advertiseService("/change_mood", &PoseCommander::changeMood, this);
		calibrateServer = nodeHandle.advertiseService("/calibrate", &PoseCommander::calibrate, this);

		averagePublisher = nodeHandle.advertise<project::Objects>("/average_objects", 1);

		homeArmClient = nodeHandle.serviceClient<project::HomeArm>("/jaco/home_arm");
		cameraToJacoClient = nodeHandle.serviceClient<project::CameraToJaco>("/camera_to_jaco");
		beginRecordingClient = nodeHandle.serviceClient<project::BeginRecording>("/begin_recording");
		endRecordingClient = nodeHandle.serviceClient<project::EndRecording>("/end_recording");
	}

	bool calibrate(project::CalibrateRequest &request, project::CalibrateResponse &response) {

		ROS_INFO("Beginning calibration");
		beginRecording();

		project::MoveArmResponse moveArmResponse;
		project::MoveArmRequest moveArmRequest;

		moveArmRequest.position.x = 0.6;
		moveArmRequest.position.y = -0.05;
		moveArmRequest.position.z = -0.1;
		moveArm(moveArmRequest, moveArmResponse);

		ros::Duration(4.0).sleep();

		moveArmRequest.position.x = 0.15;
		moveArmRequest.position.y = -0.5;
		moveArmRequest.position.z = -0.02;
		moveArm(moveArmRequest, moveArmResponse);

		ros::Duration(4.0).sleep();

		moveArmRequest.position.x = 0.4;
		moveArmRequest.position.y = -0.4;
		moveArmRequest.position.z = -0.02;
		moveArm(moveArmRequest, moveArmResponse);

		ros::Duration(4.0).sleep();

		homeArm();

		endRecording();
		ROS_INFO("Ending calibration");
	}

	bool changeMood(project::ChangeMoodRequest &request, project::ChangeMoodResponse &response) {
		for (int a = 0; a < MOOD_NUM; ++a) {
			if (request.mood == moods[a]) {
				mood = (Mood)a;
				ROS_INFO("Mood is now %d", mood);
				return true;
			}
		}
		return false;
	}

	void addObjectsToHistory(std::vector<ObjectStruct> objects) {
		
		object_index++;
		if (object_index >= OBJECT_HISTORY) {
			object_index -= OBJECT_HISTORY;
		}
		
		object_history_count++;
		if (object_history_count > OBJECT_HISTORY) {
			object_history_count = OBJECT_HISTORY;
		}

		object_history[object_index] = objects;
	}

	double distBetweenObjects(ObjectStruct a, ObjectStruct b) {
		double dx = a.x - b.x;
		double dy = a.y - b.y;
		double dz = a.z - b.z;
		return std::sqrt(dx * dx + dy * dy + dz * dz);
	}

	bool objectsHaveSettled() {

		// Check that we've seen everything
		if (object_index < 0) {
			ROS_INFO("No messages received yet.");
			return false;
		}

		// Check that we've seen enough things
		if (object_history_count < OBJECT_HISTORY) {
			ROS_INFO("Observing...");
			return false;
		}

		// Make sure that there's a consistent amount of objects
		int object_num = object_history[0].size();

		for (int a = 1; a < OBJECT_HISTORY; ++a) {
			if (object_history[a].size() != object_num) {
				ROS_INFO("Inconsistent amount of objects.");
				return false;
			}
		}

		// Skip out if there's no objects
		if (object_num == 0) {
			ROS_INFO("No objects seen");
			return false;
		}

		// Make sure that the objects haven't deviated too much
		average = object_history[0];

		for (int a = 1; a < OBJECT_HISTORY; ++a) {
			for (int b = 0; b < object_num; ++b) {

				int index = 0;
				double min = FLT_MAX;

				for (int c = 0; c < object_num; ++c) {
					double dist = distBetweenObjects(object_history[0][b], object_history[a][c]);
					if (dist < min) {
						min = dist;
						index = c;
					}
				}

				average[b] = average[b] + object_history[a][index];
			}
		}

		for (int a = 0; a < object_num; ++a) {
			average[a].x /= OBJECT_HISTORY;
			average[a].y /= OBJECT_HISTORY;
			average[a].z /= OBJECT_HISTORY;
			average[a].height /= OBJECT_HISTORY;
		}

		/*printf("Average points\n");
		for (int a = 0; a < average.size(); ++a) {
			printf("%d: (%f, %f, %f)\n", a, average[a].x, average[a].y, average[a].z);
		}*/

		std::vector<ObjectStruct> blank(object_num);
		std_dev = blank;
		for (int a = 0; a < std_dev.size(); ++a) {
			std_dev[a].x = 0;
			std_dev[a].y = 0;
			std_dev[a].z = 0;
		}

		for (int a = 0; a < OBJECT_HISTORY; ++a) {
			for (int b = 0; b < object_num; ++b) {

				double dx = object_history[a][b].x - average[b].x;
				double dy = object_history[a][b].y - average[b].y;
				double dz = object_history[a][b].z - average[b].z;

				std_dev[b].x += dx * dx;
				std_dev[b].y += dy * dy;
				std_dev[b].z += dz * dz;
			}
		}

		for (int a = 0; a < object_num; ++a) {
			std_dev[a].x = std::sqrt(std_dev[a].x/OBJECT_HISTORY);
			std_dev[a].y = std::sqrt(std_dev[a].y/OBJECT_HISTORY);
			std_dev[a].z = std::sqrt(std_dev[a].z/OBJECT_HISTORY);
			std_dev[a].height = std::sqrt(std_dev[a].height/OBJECT_HISTORY);
		}

		printf("Standard deviations\n");

		for (int a = 0; a < std_dev.size(); ++a) {
			printf("%d: (%f, %f, %f)\n", a, std_dev[a].x, std_dev[a].y, std_dev[a].z);
		}

		for (int a = 0; a < std_dev.size(); ++a) {
			if (std_dev[a].x > STD_DEV_THRESH || std_dev[a].y > STD_DEV_THRESH || std_dev[a].z > STD_DEV_THRESH) {
				ROS_INFO("Objects have not settled");
				return false;
			}
		}

		return true;
	}

	void onObjects(const project::ObjectsConstPtr &input) {

		addObjectsToHistory(structsFromObjectsMsg(*input));
		
		if (objectsHaveSettled()) {
			ROS_INFO("Performing action");
			handleAction();
		}
	}

	void handleAction() {

        int target_index = 0;

        if (mood == CREATIVE && average.size() > 1) {
            double min_height = 100000.0;
            for (int a = 0; a < average.size(); ++a) {
                if (average[a].height < min_height) {
                    min_height = average[a].height;
                    target_index = a;
                }
            }
        }

		geometry_msgs::Point target;
		cameraToJaco(average[target_index], target);

		ROS_INFO("Homing in on (%f, %f, %f)", target.x, target.y, target.z);

		if (mood == OBEDIENT) {

			ROS_INFO("Obediently remaining still");
			return;

		} else if (mood == ANGRY || mood == GREEDY || mood == OCD || mood == CURIOUS || (mood == CREATIVE && average.size() > 1)) {
			
			project::PickupRequest request;
			request.position.x = target.x;
			request.position.y = target.y;
			request.position.z = target.z + ARM_RAISE;
			project::PickupResponse response;

			pickup(request, response);

			if (!response.success) {
				ROS_ERROR("Failed to pick up object.");
				return;
			}

			if (mood == ANGRY) {
				
				doubleRelease();

			} else if (mood == OCD) {

				project::PutDownRequest request;
				request.position.x = 0.6;
				request.position.y = -0.05;
				request.position.z = target.z + ARM_RAISE;
				project::PutDownResponse response;

				putDown(request, response);

			} else if (mood == CURIOUS) {
                
				project::PutDownRequest request;
				request.position.x = target.x;
				request.position.y = target.y;
				request.position.z = target.z + ARM_RAISE;
				project::PutDownResponse response;

				putDown(request, response);
                
            } else if (mood == CREATIVE) {
                
                int base_index = 0;
                double max_height = -10000.0;

                for (int a = 0; a < average.size(); ++a) {
                    if (average[a].height > max_height) {
                        max_height = average[a].height;
                        base_index = a;
                    }
                }

                geometry_msgs::Point base;
                cameraToJaco(average[base_index], base);

				project::PutDownRequest request;
				request.position.x = base.x;
				request.position.y = base.y;
				request.position.z = base.z + average[target_index].height + ARM_RAISE + CREATIVE_GAP;
				project::PutDownResponse response;

				putDown(request, response);
            }
		}
	}

	bool beginRecording() {

		project::BeginRecording srv;
		if (!beginRecordingClient.call(srv)) {
			ROS_ERROR("Failed to begin recording.");
			return false;
		}

		return true;
	}

	bool endRecording() {

		project::EndRecording srv;
		if (!endRecordingClient.call(srv)) {
			ROS_ERROR("Failed to end recording.");
			return false;
		}

		return true;
	}

	bool homeArm() {

		project::HomeArm srv;
		if (!homeArmClient.call(srv)) {
			ROS_ERROR("Failed to home arm.");
			return false;
		}

		return true;
	}

	bool cameraToJaco(ObjectStruct object, geometry_msgs::Point &result) {

		ROS_INFO("Camera to jaco: (%f, %f, %f)", object.x, object.y, object.z);
		project::CameraToJaco srv;
		srv.request.position.x = object.x;
		srv.request.position.y = object.y;
		srv.request.position.z = object.z;

		if (!cameraToJacoClient.call(srv)) {
			ROS_INFO("Coordinate conversion failed");
			return false;
		}

		result.x = srv.response.position.x;
		result.y = srv.response.position.y;
		result.z = srv.response.position.z;

		return true;
	}

	bool doubleRelease() {

		project::ReleaseRequest releaseRequest;
		project::ReleaseResponse releaseResponse;

		for (int a = 0; a < 2; ++a) {
			release(releaseRequest, releaseResponse);
			if (!releaseResponse.success) return false;
			ros::Duration(2.0).sleep();
		}

		return true;
	}

	bool doubleGrab() {

		project::GrabRequest grabRequest;
		project::GrabResponse grabResponse;

		for (int a = 0; a < 2; ++a) {
			grab(grabRequest, grabResponse);
			if (!grabResponse.success) return false;
			ros::Duration(2.0).sleep();
		}

		return true;
	}

	bool pickup(project::PickupRequest &request, project::PickupResponse &response) {
		
		ROS_INFO("Picking up at coordinates");
		response.success = false; // Assume failure

		if (!doubleRelease()) return false;

		project::MoveArmRequest move_arm_request;
		move_arm_request.position = request.position;
		project::MoveArmResponse move_arm_response;

		moveArm(move_arm_request, move_arm_response);
		if (!move_arm_response.success) return false;
		ros::Duration(2.0).sleep();

		if (!doubleGrab()) return false;

		if (!homeArm()) return false;
		ros::Duration(2.0).sleep();

		response.success = true;

		return true;
	}

	bool putDown(project::PutDownRequest &request, project::PutDownResponse &response) {

		ROS_INFO("Putting down at coordinates");
		response.success = false; // Assume failure

		project::MoveArmRequest move_arm_request;
		move_arm_request.position = request.position;
		project::MoveArmResponse move_arm_response;

		moveArm(move_arm_request, move_arm_response);
		if (!move_arm_response.success) return false;
		ros::Duration(2.0).sleep();

		doubleRelease();

		if (!homeArm()) return false;
		ros::Duration(2.0).sleep();

		response.success = true;

		return true;
	}

	bool moveArm(project::MoveArmRequest &request, project::MoveArmResponse &response) {

		ROS_INFO("Moving arm...");

		response.success = sendMovementGoal(request.position);

		return true;
	}

	bool grab(project::GrabRequest &request, project::GrabResponse &response) {

		ROS_INFO("Grabbing");

		project::FingerPosition finger_position;
		finger_position.Finger_1 = 50;
		finger_position.Finger_2 = 50;
		finger_position.Finger_3 = 50;
		response.success = sendFingerGoal(finger_position);

		return true;
	}

	bool release(project::ReleaseRequest &request, project::ReleaseResponse &response) {
		
		ROS_INFO("Releasing");

		project::FingerPosition finger_position;
		finger_position.Finger_1 = 0;
		finger_position.Finger_2 = 0;
		finger_position.Finger_3 = 0;
		response.success = sendFingerGoal(finger_position);

		return true;
	}

	bool sendMovementGoal(geometry_msgs::Point movement_goal) {

		ROS_INFO("Homing arm...");

		if (!homeArm()) {
			return false;
		}

		ROS_INFO("Waiting for action server...");
		if (!armPoseClient.waitForServer(ros::Duration(5.0))) return false;

		ROS_INFO("Sending goal to move to (%f,%f,%f)", movement_goal.x, movement_goal.y, movement_goal.z);

		project::ArmPoseGoal goal;

		goal.pose.header.frame_id = "/jaco_api_origin";

		goal.pose.pose.position = movement_goal;

		// Face the arm downwards
		goal.pose.pose.orientation.x = 0;
		goal.pose.pose.orientation.y = 1;
		goal.pose.pose.orientation.z = 0;
		goal.pose.pose.orientation.w = 0;

		armPoseClient.sendGoal(goal);

		bool finished_before_timeout = armPoseClient.waitForResult(ros::Duration(15.0));

		if (finished_before_timeout) {
		    actionlib::SimpleClientGoalState state = armPoseClient.getState();
		    ROS_INFO("Action finished: %s", state.toString().c_str());
	    } else {
	    	ROS_INFO("Action did not finish before the time out.  Cancelling goals. ");
	    	armPoseClient.cancelAllGoals();
	    	ROS_INFO("Goals cancelled. ");
	    }

	    return finished_before_timeout;
	}

	bool sendFingerGoal(project::FingerPosition goal_position) {

		ROS_INFO("Waiting for action server...");
		if (!fingerPositionClient.waitForServer(ros::Duration(5.0))) return false;

		ROS_INFO("Sending goal to adjust fingers to (%f,%f,%f)", goal_position.Finger_1, goal_position.Finger_2, goal_position.Finger_3);

		project::SetFingersPositionGoal goal;

		goal.fingers = goal_position;

		fingerPositionClient.sendGoal(goal, 
			boost::bind(&PoseCommander::onFingersDone, this, _1, _2),
			boost::bind(&PoseCommander::onFingersActive, this),
			boost::bind(&PoseCommander::onFingersFeedback, this, _1));

	    return true;
	}

	void onFingersDone(const actionlib::SimpleClientGoalState &state, const project::SetFingersPositionResultConstPtr &result) {
	    ROS_INFO("Action finished: %s", state.toString().c_str());
	}

	void onFingersActive() {
		ROS_INFO("Fingers active");
	}

	void onFingersFeedback(const project::SetFingersPositionFeedbackConstPtr &feedback) {

		ROS_INFO("(%f, %f, %f)", feedback->fingers.Finger_1, feedback->fingers.Finger_2, feedback->fingers.Finger_3);

		if (previous_fingers.Finger_1 == feedback->fingers.Finger_1
			&& previous_fingers.Finger_2 == feedback->fingers.Finger_2
			&& previous_fingers.Finger_3 == feedback->fingers.Finger_3) {
			
			fingerPositionClient.cancelAllGoals();
			previous_fingers.Finger_1 = -1;
			previous_fingers.Finger_2 = -1;
			previous_fingers.Finger_3 = -1;
		} else {
			previous_fingers = feedback->fingers;
		}
	}
};

PLUGINLIB_DECLARE_CLASS(0, 0, PoseCommander, nodelet::Nodelet)
